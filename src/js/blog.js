let blogInputs = $('.blog-search');

if (blogInputs.length !== 0) {
    blogInputs.find('input').focus(function () {
        blogInputs.find('img').addClass('left')
        
        $(this).removeClass('hidden');
    });
    
    blogInputs.find('input').focusout(function () {
        blogInputs.find('img').removeClass("left");

        if ($(this).val()) {
            $(this).addClass('hidden')
        } 
        
        blogInputs.removeClass('active');
    });
}