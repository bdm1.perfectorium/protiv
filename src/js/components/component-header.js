let menu = $('.navbar-collapse');
let header = $('.header'),
    headerFullFlag = true;

let toggler = $('.js-toggler'),
    togglerFlag = true;

if (toggler) {
  toggler.on('click', () => {
    while (menu.hasClass('collapsing')) {
      return false;
    }
    if (toggler.hasClass('collapsed')) {
      header.addClass("collapsed");
    }
    else {
      header.removeClass("collapsed");
    }
  })
}

$(document).ready(() => {
  if (window.pageYOffset > 32) {
    header.addClass('hidden');
  }
})


var lastScrollTop = 0;
let wpAdmin = $(document).find('#wpadminbar');

$(window).on('scroll', (e) => {

  var st = window.pageYOffset || document.documentElement.scrollTop;
  
  if (window.pageYOffset > 32) {
    
    if (document.documentElement.clientWidth > 768) {
      header.addClass('hidden');
    }
    if (st < lastScrollTop ) {
      header.removeClass('hidden');
    }
      header.addClass('full');
  }
  
  else {
    header.removeClass('hidden');
    header.removeClass('full');
  }
  lastScrollTop = st <= 0 ? 0 : st;
  

  if (wpAdmin.length !== 0) {
    header.classList.add('logged-admin');
  }
});

// if (wpAdmin.length !== 0) {
//   header.classList.add('logged-admin');

//   window.addEventListener('scroll', () => {
//     if (window.pageYOffset >= 32 && document.documentElement.clientWidth < 768) {
//       header.classList.remove('logged-admin');
//     }
//     else {
//       header.classList.add('logged-admin');
//     }
//   })

// }


header.on('click', '.nav-item', function (event) {
  event.preventDefault();

  $([document.documentElement, document.body]).animate({
      scrollTop: $(`section.${$(this).attr('to')}`).offset().top + ($(this).attr('to') == 'contact' ? 200 : 0)
  }, 500);

  // header.removeClass('collapsed');
  // header.addClass('hidden');
  // toggler.attr('aria-expanded') = false;
  // toggler.addClass('collapsed');
});

$('.footer-links').on('click', 'li', function (event) {
  event.preventDefault();

  $([document.documentElement, document.body]).animate({
      scrollTop: $(`section.${$(this).attr('to')}`).offset().top + ($(this).attr('to') == 'contact' ? 200 : 0)
  }, 500);
});


$(".hero-content-links").children().each((i, item) => {
  $(item).on('click', function (e) {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(`section.${$(this).attr('to')}`).offset().top + ($(this).attr('to') == 'contact' ? 200 : 0)
    }, 500);
  })
});

// $('.btn-contact').on('click', () => {
//   $([document.documentElement, document.body]).animate({
//     scrollTop: $("section.contact").offset().top + 400
//   }, 500);
// });