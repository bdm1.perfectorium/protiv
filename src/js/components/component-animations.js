$(document).ready(() => {
    let mainTimer = setTimeout(() => {
        $('.hero-content-arrow').css({'animation-name': 'animateArrow'});
        //Right img
        $('.browser-svg, .clock-svg, .wrench-svg, .man-svg, .woman-svg').css({'animation-name': 'animateDetails'}); 
        
        $('.gear-svg').css({
            'opacity': '1'
        });
        
        let detailsTimer = setTimeout(() => {
            $('.browser-svg, .clock-svg, .gear-svg, .wrench-svg, .man-svg, .woman-svg, .hero-content-arrow').css({'opacity': '1'});
    
            let otherTimer = setTimeout(() => {
                $('.flower-svg, .chat-svg, .lamp-svg').css({'animation-name': 'animateDetails'});
                $('.gear-svg').css({'animation-name': 'rotateAnimate', 'transition': 'none'});
                
                let thirdTimer = setTimeout(() => {
                    $('.flower-svg, .chat-svg, .lamp-svg').css({'opacity': '1'});
                    $('.chat-svg-line, .lamp-svg-line, .flower-svg-line').css({'animation-name': 'linesAnimate'});
                    clearTimeout(thirdTimer);
                }, 150);
                
                clearTimeout(otherTimer)
            }, 550);  
            
            clearTimeout(detailsTimer)
        }, 350);
        
        clearTimeout(mainTimer)
    }, 300);

    var caseStudyAnimateFlag = true;
    if ($('.case-study-content').length !== 0) {
        if (elementIsVisible($('.case-study-content')) && caseStudyAnimateFlag) {
          caseStudyAnimateFlag = false;
          $('.case-study-content-img-line').css({
            'animation-name': 'imgAnimate'
          });
          $(".case-study-content-img-gear").css({
            'animation-name': 'gearAnimate'
          });
          $(".case-study-content-img-chart").css({
            'animation-name': 'chartAnimate'
          });
        } else {
          $(window).on('scroll', function () {
            if (elementIsVisible($('.case-study-content')) && caseStudyAnimateFlag) {
              caseStudyAnimateFlag = false;
              $('.case-study-content-img-line').css({
                'animation-name': 'imgAnimate'
              });
              $(".case-study-content-img-gear").css({
                'animation-name': 'gearAnimate'
              });
              $(".case-study-content-img-chart").css({
                'animation-name': 'chartAnimate'
              });
            }
          });
        }
      }
})