let tags = $('.industries-tags-wrap');

function animationTags(item, i) {
    let destination = (i % 2 == 0) ? '-' : '+';

    let children = '',
        childrenLength = $(item).children().length;

    $(item).children().each((_, el) => {
        children += el.outerHTML;
    });

    if (destination == '-') {
        $(item).append(children);
    }
    else {
        $(item).prepend(children);
    }
    $('.industries-tags').children().each((index, _) => $('.industries-tags-wrap').get(index).style.setProperty('--count', childrenLength));

    $(item).css({
        'animation-name': destination == '+' ? 'loopIndustriesRight' : 'loopIndustriesLeft'
    });
}

$(document).ready(() => {
    if (tags.length !== 0) {
        tags.each((i, item) => animationTags(item, i));
    }
});
