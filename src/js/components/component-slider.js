
/* SLIDERS SETTINGS */

let slider = $('.slider-wrap');

function initSlider(slidesToDevide) {
  slider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    infinite: false,
    variableWidth: true
    // vertical: true,
    // verticalSwiping: true
  });
  
  slider.on('beforeChange', () => {
    $('[data-toggle="popover"]').popover('hide');
  });

  // function switchSlides (e) {

  // }
  
  // var scrollTrigger = true;
  
  // if ($(window).width() > 992) {

  //   slider.find('.slider-item').on({
  //     'wheel': (e) => switchSlides(e),
  //     // 'scroll': (e) => switchSlides(e)
  //   });

  // }

  $('.slider-wrap').on('beforeChange', (event, slick, currentSlide, nextSlide) => {
    if (nextSlide == 1) {
      flags.first = false;
      renderUnits(diagramContainer);
    }
    if (nextSlide == 2) {
      flags.second = false;
      renderUnits(diagramContainerSecond);
    }
  });

  if (slidesToDevide) {
    Object.keys(slidesToDevide).forEach((key) => {
      if (key == '0' || key == '1') {
        $(".slick-dots").find(`li:nth-child(${parseInt(slidesToDevide[key].dataset.slickIndex) + 2})`).css({'display': 'none'});
        
        
        slider.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
          // console.log(currentSlide, nextSlide, $(`.slick-dots li:nth-child(${nextSlide})`))

          if (slidesToDevide[key].dataset.slickIndex + 1 == nextSlide) {
            
            $(".slick-dots").find(`li:nth-child(${nextSlide}`).addClass('mobile-active');
          }
          else {
            $('.slick-dots').find(`li:nth-child(${currentSlide})`).removeClass('mobile-active');
          }
        });
      }
    });
  }

  $(".slick-dots li").each((i, item) => {
    if (i == 0) {
      $(item).html(`<p>Overview</p><button></button>`);
    }
    else if (i == $('.slick-dots li').length - 1) {
      $(item).html(`<p>Results</p><button></button>`);
    }
    else {
      if (slidesToDevide) {
        $(item).html(`<p>Step ${i - 1}</p><button></button>`); 
      }
      else {
        $(item).html(`<p>Step ${i}</p><button></button>`);
      }
    }
  });
}

function remakeForMobile() {
  let slidesToDevide = slider.find('.devide');
  
  slidesToDevide.each((i, item) => {
    let itemSecondColumn = $(item).find('.slider-item-column')[1];
    
    let subClass = () => {
      if ($(item).attr('class').split(' ')[1] && $(item).attr('class').split(' ')[1] !== 'devide') {
        return $(item).attr('class').split(' ')[1];
      }
      else {
        return;
      }
    }

    const dots = document.createElement('ul');
    dots.classList.add('slider-item-dots');
    
    for (let i = 0; i < 2; i++) {
      const li = document.createElement('li');
      dots.appendChild(li);
    }

    // Remove second column from item
    itemSecondColumn.remove();

    dots.classList.add('second');
    
    // Append to second column dots
    itemSecondColumn.append(dots);
    
    // Paste Second column after first
    $(item).after(`<div class="slider-item ${subClass()}">${itemSecondColumn.outerHTML}</div>`);
    
    dots.classList.remove('second');
    dots.classList.add('first');
    
    $(item).find('.slider-item-column').append(dots);

    if ($(item).find('.roadmap').length) {
      $(item).find('.slider-item-column').addClass('column-roadmap');
    }
    // $(item).find('.slider-item-column')[0].append(dots)
    
  });

  initSlider(slidesToDevide);
}

var scrollingFlag = true;

var scrollCount = null;
var scroll= null;


function heroScroll() {
  $('section.hero').on('wheel', (e) => {
    
    if (scrollingFlag && e.originalEvent.deltaY > 0) {
      e.preventDefault();
      scrollingFlag = false;

      $([document.documentElement, document.body]).animate({
        scrollTop: $(".slider-wrap").offset().top
      }, 500, () => {
        scrollingFlag = true;
      });
    }
  });
}

function sliderScroll() {
  slider.find('.slider-item').on('mousewheel DOMMouseScroll wheel', function (e) {
    
    e.preventDefault();

    clearTimeout(scroll);
    scroll = setTimeout(function(){scrollCount=0;}, 200);
    if(scrollCount) return 0;
    scrollCount=1;
    // let deltaY = e.originalEvent.deltaY;
    // e.preventDefault();
    // e.stopPropagation();

    // clearTimeout(blockTimeout);
    // blockTimeout = setTimeout(function(){
    //   blocked = false;
    // }, 150);


    if (scrollingFlag) {
      scrollingFlag = false;

      if (e.originalEvent.deltaY == 100) {
        if (slider.slick('slickCurrentSlide') == slider.find('.slick-track').children().length - 1) {
          $([document.documentElement, document.body]).animate({
            scrollTop: $("section.use").offset().top
          }, 500);
        }
        else {
          slider.slick('slickNext');
        }   
        setTimeout(() => scrollingFlag = true, 400);
      
      }
      else if (e.originalEvent.deltaY == -100) {
        if (slider.slick('slickCurrentSlide') == 0) {
          $([document.documentElement, document.body]).animate({
            scrollTop: $("section.hero").offset().top
          }, 500);
        }
        else {
          slider.slick('slickPrev');
        }
        
        setTimeout(() => scrollingFlag = true, 400);
      }
    }
  });
}

$(document).ready(() => {
  if (slider.length !== 0) {
    if ($(window).width() < 992) {
      remakeForMobile();
    }

    else {
      initSlider();
      heroScroll();
      sliderScroll();
    }
  }
})