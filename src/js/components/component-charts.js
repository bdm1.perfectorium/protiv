const style = (item, prop, color) => {
  if (Array.isArray(prop)) {
    return prop.forEach((elem, i) => {
      $(item).css(elem[0], elem[1])  
    })
  }
  else {
    return $(item).css(
      prop, color
    )
  }
}

const colors = [
  '#3E8BFF', '#14386D', '#85A2CC', '#5FBC78'
];

var diagramContainer = $('.diagram-1'),
    diagramContainerSecond = $('.diagram-2');

var flags = {
  first: true,
  second: true   
} 

function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();
  return elemBottom <= docViewBottom && elemTop >= docViewTop;
}

function initDiagram(diagram, flag) {
  if (diagram && isScrolledIntoView(diagram) && flags[flag] == true) {
    flags[flag] = false;
    renderUnits(diagram);
  }
}

function showDiagram(diagram, flag) {
  initDiagram(diagram, flag);
  window.addEventListener('scroll', function () {
    initDiagram(diagram, flag);
  });
}

function offsets(diagram, id) {
  var result = 0;

  for (var i = id; i >= 0; i--) {
    if (diagram.find('.diagram-wrap-chart-units').children()[i].localName == 'circle') {
      result += parseInt(diagram.find('.diagram-wrap-chart-units').children()[i].dataset.percent);
    }
  }
  return result;
}

function renderUnits(diagram) {
  
  diagram.find('.diagram-wrap-chart-units').children().each(function (index, item) {
    if (item.localName == 'circle') {

      var offset = offsets(diagram, index - 1);

      style(item, [
        ['display', 'block'],
        ['stroke', $(item).attr('data-color')],
        ['strokeDasharray', "".concat($(item).attr('data-percent'), " 100")],
        ['strokeDashoffset', "-".concat(offset)],
      ]);
    }
  });

  diagram.find('.diagram-label').each((i, item) => {
    style($(item), 'cursor', 'pointer');
    style($(item).find('.diagram-label-percent, .diagram-label-title-badge'), 'background-color', $(item).attr('data-color'))
    
    let diagramCircle = $(diagram.find('circle')[i]),
        strokeWidthInitial = diagramCircle.css("stroke-width")
        
      $(item).on('mouseenter', (e) => {
        style(diagramCircle, 'stroke-width', '5px')
      });
      
      $(item).on('mouseleave', (e) => {
        style(diagramCircle, 'stroke-width', strokeWidthInitial)
      });
  });
}



$('.diagram-labels.unit').children().each((index, item) => {
  $(item).find('.diagram-label-percent').css({
    'background-color': $(item).attr('data-color') 
  })
})