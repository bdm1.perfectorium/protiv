let triggers = $('.case-study-views-item'),
    read_more = $('.case-study-content-more-trigger');

triggers.each((i, item) => {
    $(item).on('click', (e) => {
        if (!$(item).hasClass('active')) {
            $('.case-study-content.active').removeClass('active');
            $('.case-study-views-item.active').removeClass('active');
            
            $(item).addClass('active');
            $(`.case-study-content[data-view=${$(item).attr('data-view')}]`).addClass('active');

        }
    })
})

read_more.on('click', () => {
    if ($(`.case-study-content-more-content, .case-study-content-more-trigger`).hasClass('active')) {
        $(`.case-study-content-more-content, .case-study-content-more-trigger`).removeClass('active')
    }
    else {
        $(`.case-study-content-more-content, .case-study-content-more-trigger`).addClass('active');
    }
})