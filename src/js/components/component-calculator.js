function initCalculator() {
    var values = {
        'annual': 0,
        'direct_num': 0,
        'direct_percent': 0,
        'hourly': 0,
        'current_yearly': 0,
        'yearly': 0
      };
      var isFilled = false;
      var constants = {
        growth_rate_percent: [5, 10, 15],
        labor_savings_percent: [2.5, 6, 12],
        burden_rate_percent: 25,
        billable_rate: 109.38
      };
      var results = {
        billable_rate: [],
        employee_wage_growth: [],
        labor_savings: [],
        capacity_growth_hours: [],
        capacity_growth_revenue: [],
        capacity_growth_profit: [],
        financial_gains: []
      };
      var calcBySelect = {
        'financial_gains': {
          'title': 'Potential Financial Gain',
          "subtitle": "Percentage represents the Financial Gains / Annual Revenue",
          'list': 'financial_gains'
        },
        'employee_wage_growth': {
          'title': 'Employee Wage Growth',
          "subtitle": 'Percentage represents the Financial Gains / Annual Revenue',
          'list': 'employee_wage_growth'
        },
        'capacity_growth_hours': {
          'title': 'Capacity Growth Hours & Revenue',
          "subtitle": 'Percentage represents the Financial Gains / Annual Revenue',
          'list': 'capacity_growth_hours'
        }
      };
      var myChart = null;
  
      function financial(x) {
        return Number.parseFloat(x).toFixed(2);
      }
  
      function calculate() {
        var newResults = {...results};
  
        for (var i = 0; i < 3; i++) {
          var intermediateVal = 0;
          newResults.billable_rate[i] = Number(((values.hourly * constants.burden_rate_percent / 100 + values.hourly) / values.direct_percent * 100).toFixed(2));
          intermediateVal = Number(values.hourly * constants.growth_rate_percent[i] / 100 + values.hourly);
          newResults.employee_wage_growth[i] = {
            value: Math.floor(intermediateVal),
            percent: Math.floor((intermediateVal - values.hourly) / values.hourly * 100)
          };
          intermediateVal = values.direct_num * constants.labor_savings_percent[i] / 100;
          newResults.labor_savings[i] = {
            value: Math.floor(intermediateVal),
            percent: Math.floor(intermediateVal / values.annual * 100)
          };
          intermediateVal = Math.floor(values.direct_num * constants.labor_savings_percent[i] / 100 / (values.hourly * constants.burden_rate_percent / 100 + values.hourly));
          newResults.capacity_growth_hours[i] = {
            value: intermediateVal,
            percent: null
          };
          intermediateVal = Math.ceil(newResults.capacity_growth_hours[i].value * newResults.billable_rate[i] / 100) * 100;
          newResults.capacity_growth_revenue[i] = {
            value: intermediateVal,
            percent: Math.ceil(intermediateVal / values.annual * 100)
          };
          intermediateVal = Math.ceil(newResults.capacity_growth_revenue[i].value * values.current_yearly / 100);
          newResults.capacity_growth_profit[i] = {
            value: intermediateVal,
            percent: Math.ceil(intermediateVal / newResults.capacity_growth_revenue[i].value * 100)
          };
          intermediateVal = Number(newResults.capacity_growth_profit[i].value + newResults.labor_savings[i].value);
          newResults.financial_gains[i] = {
            value: Math.ceil(intermediateVal),
            percent: Number((intermediateVal / values.annual * 100).toFixed(2))
          };
        }
  
        results = newResults;
        return results;
      }
  
      function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
  
      function animateRange(range, val, rangeInterval) {
        val = parseInt(val);
  
        if (val > range.val()) {
          range.val(parseInt(range.val()) + range.val() / 10);
  
          if (val < range.val()) {
            range.val(val);
            clearInterval(rangeInterval);
            return;
          }
        } else if (val < range.val()) {
          range.val(parseInt(range.val()) - range.val() / 10);
  
          if (val > range.val()) {
            range.val(val);
            clearInterval(rangeInterval);
            return;
          }
        } else {
          clearInterval(rangeInterval);
          return;
        }
      }
  
      function animateField(item, fromItemValue, itemValue, onEnd) {
        item.prop('Counter', fromItemValue).animate({
          Counter: itemValue
        }, {
          duration: 1000,
          easing: 'swing',
          step: function step(now) {
            onEnd(now);
          }
        });
      }
  
  
      $('.calculator-wrap').find('.enter-field').each(function (i, item) {
        var $this = $(item),
          container = $this.closest('.calculator-enter-item'),
          range = container.find('.calculator-range');
        values[$this.attr('name')] = Number($this.val());
        isFilled = true;
        $this.val("".concat($this.hasClass('dollar') && !$this.val().includes('$') ? '$ ' : '').concat(numberWithCommas($this.val())));
        var directNum = $('.calculator-enter').find('input[name="direct_num"]');
  
        if (range.length) {
          var listener = $this.hasClass('percent') ? 'input' : 'change';
          range.on(listener, function (e) {
            values[range.attr('name')] = Number(parseInt(range.val()));
  
            if (listener == 'input') {
              container.find("h2[name=\"".concat($this.attr('name'), "\"]")).text(Math.ceil(range.val()) + '%');
  
              if (range.attr('name') == 'direct_percent') {
                values['direct_num'] = Number(values['annual'] * range.val() / 100);
                directNum.val('$' + numberWithCommas(Math.ceil(values['direct_num'])));
              }
            }
          });
        }
  
        var checkOnString = function checkOnString(val) {
          var template = new RegExp(/[^0-9,.$\s]/g);
  
          if (template.test(val)) {
            return false;
          } else if (val < 0 || !val.length) {
            return false;
          } else if (val.replace('$ ', '').trim() == '' || val.replace(', ', '').trim() == '' || val.trim() == '' || val.trim() == '$') {
            return false;
          } else {
            return true;
          }
        };
  
        $this.on('input', function (e) {
          var value = $this.val();
          isFilled = false;
  
          if (checkOnString(value) == false) {
            $this.addClass('danger');
            isFilled = false;
            values[$this.attr('name')] = null;
  
            if ($this.attr('name') == 'annual') {
              directNum.val('$ ' + 0);
            }
  
            return false;
          } else {
            $this.removeClass('danger');
            value = value.replace(/[^0-9]/g, '');
            isFilled = true;
          }
  
          values[$this.attr('name')] = Number(value);
  
          if ($this.attr('name') == 'annual') {
            values['direct_num'] = Number(value * values['direct_percent'] / 100);
            directNum.val('$' + numberWithCommas(Math.ceil(values['direct_num'])));
          }
        });
        $this.on('change', function () {
          if (values[$this.attr('name')] !== null) {
            $this.val("".concat($this.hasClass('dollar') && !$this.val().includes('$') ? '$ ' : '').concat(numberWithCommas($this.val())));
          }
        });
      });
  
      function to(step, calcBy, direction) {
        $('.calculator-details-table-row-item.name').popover('hide');
  
        switch (step) {
          case 'select':
            if (!isFilled) {
              return;
            }
  
            if ($(window).width() > 1200) {
              $('.calculator-enter').addClass('disabled');
              $('.calculator-select').removeClass('disabled');
            }
  
  
            $('.calculator-column.left').addClass('hidden');
            $('.calculator-chart').addClass('hidden');
            $('.calculator-select').removeClass('hidden');
  
            return;
  
          case 'charts':
            var title = $('.calculator-chart-title').find('h4'),
              subtitle = $('.calculator-chart-title').find('p');
  
            if (calcBy) {
              resetChart();
              initCalculatorChart(calcBy);
              results[calcBySelect[calcBy]['list']].forEach(function (item) {
                if (String(item).length >= 8) {
                  $('.calculator-results-value').addClass('long');
                  return;
                } else {
                  $('.calculator-results-value').removeClass('long');
                  return;
                }
              });
              title.text(calcBySelect[calcBy]['title']);
              subtitle.text(calcBySelect[calcBy]['subtitle']);
              $('.calculator-results-value').each(function (i, elem) {
                $(elem).find('h3').text(numberWithCommas(results[calcBySelect[calcBy]['list']][i]['value']));
              });
            }
  
            $('.calculator-enter').addClass('disabled');
            $('.calculator-select').addClass('hidden');
            $('.calculator-chart').removeClass('hidden');
            $('.calculator-details').addClass('hidden');
  
            if (document.documentElement.clientWidth > 1200) {
              $('.calculator-column.left').removeClass('hidden');
            } else {
              $(".calculator-wrap").addClass("spaced");
            }
  
            $('.calculator-column.first').removeClass('slide');
            $('.calculator-column.second').addClass('hidden');
            $('.calculator-chart').find('.calculator-btns').removeClass('full');
            return;
  
          case 'view':
            Object.keys(results).forEach(function (item) {
              if (Array.isArray(results[item])) {
                results[item].forEach(function (elem) {
                  if (String(elem.value) > 9) {
                    $('.calculator-details-table-row-item').addClass('long');
                    return;
                  }
  
                  return;
                });
                return;
              }
  
              return;
            });
            $('.calculator-details-table-row').each(function (i, elem) {
              if (!$(elem).hasClass('.head-info')) {
                $(elem).children().each(function (index, item) {
                  if ($(item).find('h4').length !== 0) {
                    if ($(item).hasClass('percent')) {
                      $(item).find('h4').text(numberWithCommas(results[$(elem).attr('name')][index - 2]['percent']) + ' %');
                    } else if ($(item).hasClass('dollar')) {
                      $(item).find('h4').text('$ ' + numberWithCommas(results[$(elem).attr('name')][index - 2]['value']));
                    } else if ($(item).hasClass('hours')) {
                      $(item).find('h4').text(numberWithCommas(results[$(elem).attr('name')][index - 2]['value']) + ' h');
                    }
  
                  } else if ($(item).hasClass('name')) {
                    $(item).on('mouseenter', function () {
                      $(item).popover({
                        template: "<div class=\"popover show fade bs-popover-top right\" role=\"tooltip\" id=\"popover163477\" x-placement=\"top\" style=\"position: absolute; transform: translate3d(648px, 821px, 0px); top: 0px; left: 0px; will-change: transform;\"><div class=\"arrow\" style=\"left: 124px;\"></div><div class=\"popover-body\">".concat($(item).find('.calculator-details-table-row-item-popover').attr('data-content'), "</div></div>")
                      }).popover("show");
                    });
                    $(item).on('mouseleave', function () {
                      $(item).popover("hide");
                    });
                  }
                });
              }
            });
            $('.calculator-column.left').addClass('hidden');
            $('.calculator-column.first').addClass('slide');
  
            if (document.documentElement.clientWidth < 1200) {
              $('.calculator-column.first').addClass('hidden');
            }
  
            $('.calculator-column.second').removeClass('hidden');
            $('.calculator-chart').find('.calculator-btns').addClass('full');
            setTimeout(function () {
              return $('.calculator-details').removeClass('hidden');
            }, 500);
            return;
  
          case 'start':
            if ($(window).width() > 1200) {
              $('.calculator-enter').removeClass('disabled');
              $('.calculator-select').addClass('disabled');
            }
  
  
            $('.calculator-column.left').removeClass('hidden');
            $('.calculator-column.right.second').addClass('hidden');
            $('.calculator-select').addClass('hidden');
            $('.calculator-chart').addClass('hidden');
            $('.calculator-enter').removeClass('disabled');
  
            $('.calculator-select').removeClass('hidden');
  
            return;
  
          default:
            break;
        }
      }
  
      var chartData = {
        'barColors': [],
        'legendColors': ['#FA681A', '#FBB040', '#5FBC78'],
        sizes: {
          canvasHeight: document.documentElement.clientWidth > 540 ? 340 : 280,
          barSize: document.documentElement.clientWidth > 540 ? '12' : '12',
          barThickness: document.documentElement.clientWidth > 540 ? 53 : 45,
          barLabelOffset: document.documentElement.clientWidth > 540 ? 5 : 5
        }
      };
      var chart = $('.calculator-chart-wrap');
  
      function initCalculatorChart(calcBy) {
        var data = {
          original: [],
          percents: []
        };
        var gridText = '';
  
        function dataToPercent(prop) {
          var list = results[calcBy].map(function (val) {
            data.original.push(val[prop]);
            return val[prop];
          });
          var total = 0;
  
          for (var i = 0; i < list.length; i++) {
            total += list[i];
          }
  
          return list.map(function (curr) {
            data.percents.push(parseFloat((curr * 100 / total).toFixed(2)));
          });
        }
  
        if (chart.hasClass('twins')) {
          chart.removeClass('twins');
        }
  
        if (calcBy == Object.keys(calcBySelect)[0]) {
          dataToPercent('value');
          gridText = 'Financial Gain';
        } else if (calcBy == Object.keys(calcBySelect)[1]) {
          dataToPercent('value');
          gridText = 'Wage Grows';
        } else if (calcBy == Object.keys(calcBySelect)[2]) {
          chart.addClass('twins');
          dataToPercent('value');
          gridText = 'Hours & Revenue';
        }
  
        if (data.percents.length) {
          if (calcBy == Object.keys(calcBySelect)[1]) {
            data.percents.map(function (item, itemIndex) {
              if (itemIndex > 0) {
                data.percents[itemIndex] = (item += data.percents[itemIndex - 1]) - 15;
              }
  
              return item;
            });
          }
  
          chart.find('.calculator-chart-grid-title span').text(gridText);
          $(chart.find('.calculator-chart-bar')).each(function (i, elem) {
            $(elem).find('.bar-column').each(function (index, column) {
              $(column).animate({
                height: index == 0 ? data.percents[i] + 15 + '%' : data.percents[i] + '%'
              }, 300);
            });
            $(elem).find('.bar-label span:nth-child(2)').remove();
  
            if (calcBy == Object.keys(calcBySelect)[0]) {
              $(elem).find('.bar-label span').text("".concat(results[calcBy][i].percent, " %"));
              $(elem).find('.bar-label').append("<span>$ ".concat(numberWithCommas(results[calcBy][i].value), "</span>"));
              $(elem).find(".bar-label span:nth-child(2)").text('$ ' + numberWithCommas(results[calcBy][i].value));
            } else if (calcBy == Object.keys(calcBySelect)[1]) {
              $(elem).find('.bar-label span').text("% ".concat(numberWithCommas(results[calcBy][i].percent)));
            } else if (calcBy == Object.keys(calcBySelect)[2]) {
              $(elem).find('.calculator-chart-bar-wrap').each(function (index, column) {
                $(column).find('.bar-label span').text(index == 0 ? "$ ".concat(numberWithCommas(results['capacity_growth_revenue'][i].value)) : "".concat(numberWithCommas(results['capacity_growth_hours'][i].value), " h"));
              });
            }
          });
        }
  
      }
  
      var chartWrap = $('.calculator-column.right.first');
  
      function resetChart() {
        $(chart).find('.calculator-chart-bar .bar-column').each(function (i, elem) {
          $(elem).css({
            height: '0%'
          });
        });
      }
  
      $('.calculator-select-item').each(function (i, item) {
        $(item).on('click', function (e) {
          calculate();
          to('charts', $(item).attr('name'));
  
          if (document.documentElement.clientWidth < 1200) {
            $(".calculator-btns-mobile").find('.to-start').addClass('hidden');
            $(".calculator-btns-mobile").find('.to-view').first().removeClass('hidden');
            $(".calculator-btns-mobile").find('.to-select.back').removeClass('hidden');
          }
        });
      });
  
      $('.calculator-btn').each(function (index, item) {
        if ($(item).hasClass('view')) {
          $(item).on('click', function () {
            to('view');
          });
        } else if ($(item).hasClass('full')) {
          $(item).on('click', function () {
            to('charts');
          });
        } else if ($(item).hasClass('to-start')) {
          $(item).on('click', function () {
            to('start');
          });
        } else if ($(item).hasClass('to-select')) {
          $(item).on('click', function () {
            if ($(item).hasClass('back')) {
              to('select', null, 'back');
            } else {
              to('select');
            }
          });
        }
  
      });
      $('.btn-contact').on('click', function () {
        scrollToElement($('section.contact'));
      });
  
      $(".calculator-btns-mobile").children().each(function (index, item) {
        $(item).on('click', function () {
          if (!isFilled) {
            return;
          }
  
          if (!$(item).hasClass('to-contact')) {
            $(item).addClass('hidden');
          }
  
          if ($(item).hasClass('to-select')) {
            to('select');
  
            if ($(item).hasClass('back')) {
              $(".calculator-chart").addClass("hidden");
              $(".calculator-btns-mobile").find('.to-view').addClass('hidden');
              $(".calculator-wrap").removeClass("spaced");
            }
  
            $(".calculator-btns-mobile").find('.to-start').removeClass('hidden');
          } else if ($(item).hasClass('to-charts')) {
            if ($(item).hasClass('back')) {
              $(".calculator-btns-mobile").find('.to-contact').addClass('hidden');
              $(".calculator-btns-mobile").find('.to-view').removeClass('hidden');
              $(".calculator-btns-mobile").find('.to-select.back').removeClass('hidden');
              $(".calculator-column.right.first").removeClass('hidden');
            } else {
              $(".calculator-btns-mobile").find('.to-contact').addClass('hidden');
              $(".calculator-btns-mobile").find('.to-view').removeClass('hidden');
              $(".calculator-btns-mobile").find('.to-select').second().removeClass('hidden');
            }
  
            to('charts');
          } else if ($(item).hasClass('to-start')) {
            to('start');
            $(".calculator-btns-mobile").find('.to-select').first().removeClass('hidden');
          } else if ($(item).hasClass('to-view')) {
            to('view');
            $(".calculator-btns-mobile").find('.to-select').addClass('hidden');
            $(".calculator-btns-mobile").find('.to-charts').removeClass('hidden');
            $(".calculator-btns-mobile").find('.to-contact').removeClass('hidden');
          }
        });
      });
}

let calculator = $("#calculator");

if (calculator.length !== 0) {
  initCalculator();
}